package com.jourwon.spring.boot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 手机品牌枚举
 * 通过本地缓存，加快枚举类的检索
 *
 * @author JourWon
 * @date 2023/4/5
 */
@Getter
@AllArgsConstructor
public enum MobilePhoneBrandEnum {

    /**
     * 苹果
     */
    APPLE("Apple", "苹果"),

    /**
     * 华为
     */
    HUAWEI("Huawei", "华为"),

    /**
     * 小米
     */
    XIAOMI("Xiaomi", "小米");

    /**
     * 品牌编码
     */
    private final String brandCode;

    /**
     * 品牌名称
     */
    private final String brandName;

    /**
     * 手机品牌jvm缓存，Map<手机品牌编码,手机品牌枚举>
     */
    public static final Map<String, MobilePhoneBrandEnum> MAP;

    static {
        MAP = Arrays.stream(MobilePhoneBrandEnum.values())
                .collect(Collectors.toMap(MobilePhoneBrandEnum::getBrandCode, Function.identity()));
    }

    /**
     * 通过品牌编码获取手机品牌枚举
     *
     * @param brandCode 品牌编码
     * @return MobilePhoneBrandEnum 手机品牌枚举
     */
    public static MobilePhoneBrandEnum getByCode(String brandCode) {
        final MobilePhoneBrandEnum mobilePhoneBrandEnum = MAP.get(brandCode);
        Assert.notNull(mobilePhoneBrandEnum, String.format("不合法的手机品牌编码[%s]", brandCode));
        return mobilePhoneBrandEnum;
    }

    /**
     * 是否合法的手机品牌编码
     *
     * @param brandCode 品牌编码
     * @return true-合法;false-非法
     */
    public static Boolean isValidBrandCode(String brandCode) {
        return MAP.containsKey(brandCode);
    }

}
