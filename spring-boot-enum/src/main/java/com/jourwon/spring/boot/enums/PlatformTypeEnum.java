package com.jourwon.spring.boot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * 平台类型
 *
 * @author JourWon
 * @date 2023/7/21
 */
@Getter
@AllArgsConstructor
public enum PlatformTypeEnum {
    /**
     * 类型1
     */
    TYPE1(1),

    /**
     * 类型2
     */
    TYPE2(2);

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 根据编码获取枚举
     *
     * @param code 编码
     * @return PlatformTypeEnum 枚举
     */
    public static PlatformTypeEnum getByCode(Integer code) {
        return Arrays.stream(values()).filter(item -> Objects.equals(item.getCode(), code)).findAny().orElse(TYPE1);
    }

}
