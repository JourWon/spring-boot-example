package com.jourwon.spring.boot.service;

/**
 * @author JourWon
 * @date 2022/6/1
 */
public interface FieldInfoService {

    /**
     * 字段名称
     *
     * @return String 字段名称
     */
    String getFieldName();

    /**
     * 字段描述
     *
     * @return String 字段描述
     */
    String getFieldDesc();

}
