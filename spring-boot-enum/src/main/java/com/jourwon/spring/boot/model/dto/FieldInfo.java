package com.jourwon.spring.boot.model.dto;

import com.jourwon.spring.boot.service.FieldInfoService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author JourWon
 * @date 2022/1/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldInfo {

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段描述
     */
    private String fieldDesc;

    public FieldInfo(FieldInfoService fieldInfoService) {
        this.fieldName = fieldInfoService.getFieldName();
        this.fieldDesc = fieldInfoService.getFieldDesc();
    }
}
