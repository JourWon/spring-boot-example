package com.jourwon.spring.boot.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 枚举缓存
 * 1、代码简洁；
 * 2、枚举中实例数越多，缓存模式的性能优势越多；
 *
 * @author JourWon
 * @date 2023/7/5
 */
public class EnumCache {

    /**
     * 以枚举任意值构建的缓存结构
     **/
    private static final Map<Class<? extends Enum>, Map<Object, Enum>> CACHE_BY_VALUE = new ConcurrentHashMap<>();
    /**
     * 以枚举名称构建的缓存结构
     **/
    private static final Map<Class<? extends Enum>, Map<Object, Enum>> CACHE_BY_NAME = new ConcurrentHashMap<>();
    /**
     * 枚举静态块加载标识缓存结构
     */
    private static final Map<Class<? extends Enum>, Boolean> LOADED = new ConcurrentHashMap<>();


    /**
     * 以枚举名称构建缓存，在枚举的静态块里面调用
     *
     * @param clazz
     * @param es
     * @param <E>
     */
    public static <E extends Enum> void registerByName(Class<E> clazz, E[] es) {
        Map<Object, Enum> map = new ConcurrentHashMap<>(16);
        for (E e : es) {
            map.put(e.name(), e);
        }
        CACHE_BY_NAME.put(clazz, map);
    }

    /**
     * 以枚举转换出的任意值构建缓存，在枚举的静态块里面调用
     *
     * @param clazz
     * @param es
     * @param enumMapping
     * @param <E>
     */
    public static <E extends Enum> void registerByValue(Class<E> clazz, E[] es, EnumMapping<E> enumMapping) {
        if (CACHE_BY_VALUE.containsKey(clazz)) {
            throw new RuntimeException(String.format("枚举%s已经构建过value缓存,不允许重复构建", clazz.getSimpleName()));
        }
        Map<Object, Enum> map = new ConcurrentHashMap<>(16);
        for (E e : es) {
            Object value = enumMapping.value(e);
            if (map.containsKey(value)) {
                throw new RuntimeException(String.format("枚举%s存在相同的值%s映射同一个枚举%s.%s", clazz.getSimpleName(), value, clazz.getSimpleName(), e));
            }
            map.put(value, e);
        }
        CACHE_BY_VALUE.put(clazz, map);
    }

    /**
     * 从以枚举名称构建的缓存中通过枚举名获取枚举
     *
     * @param clazz
     * @param name
     * @param <E>
     * @return
     */
    public static <E extends Enum> E findByName(Class<E> clazz, String name) {
        return find(clazz, name, CACHE_BY_NAME, null);
    }

    /**
     * 从以枚举名称构建的缓存中通过枚举名获取枚举
     *
     * @param clazz
     * @param name
     * @param defaultEnum
     * @param <E>
     * @return
     */
    public static <E extends Enum> E findByName(Class<E> clazz, String name, E defaultEnum) {
        return find(clazz, name, CACHE_BY_NAME, defaultEnum);
    }

    /**
     * 从以枚举转换值构建的缓存中通过枚举转换值获取枚举
     *
     * @param clazz
     * @param value
     * @param <E>
     * @return
     */
    public static <E extends Enum> E findByValue(Class<E> clazz, Object value) {
        return find(clazz, value, CACHE_BY_VALUE, null);
    }

    /**
     * 从以枚举转换值构建的缓存中通过枚举转换值获取枚举
     *
     * @param clazz
     * @param value
     * @param defaultEnum
     * @param <E>
     * @return
     */
    public static <E extends Enum> E findByValue(Class<E> clazz, Object value, E defaultEnum) {
        return find(clazz, value, CACHE_BY_VALUE, defaultEnum);
    }

    private static <E extends Enum> E find(Class<E> clazz, Object obj, Map<Class<? extends Enum>, Map<Object, Enum>> cache, E defaultEnum) {
        Map<Object, Enum> map;
        if ((map = cache.get(clazz)) == null) {
            // 触发枚举静态块执行
            executeEnumStatic(clazz);
            // 执行枚举静态块后重新获取缓存
            map = cache.get(clazz);
        }
        if (map == null) {
            String msg = null;
            if (cache == CACHE_BY_NAME) {
                msg = String.format(
                        "枚举%s还没有注册到枚举缓存中，请在%s.static代码块中加入如下代码 : EnumCache.registerByName(%s.class, %s.values());",
                        clazz.getSimpleName(),
                        clazz.getSimpleName(),
                        clazz.getSimpleName(),
                        clazz.getSimpleName()
                );
            }
            if (cache == CACHE_BY_VALUE) {
                msg = String.format(
                        "枚举%s还没有注册到枚举缓存中，请在%s.static代码块中加入如下代码 : EnumCache.registerByValue(%s.class, %s.values(), %s::getXxx);",
                        clazz.getSimpleName(),
                        clazz.getSimpleName(),
                        clazz.getSimpleName(),
                        clazz.getSimpleName(),
                        clazz.getSimpleName()
                );
            }
            throw new RuntimeException(msg);
        }
        if (obj == null) {
            return defaultEnum;
        }
        Enum result = map.get(obj);
        return result == null ? defaultEnum : (E) result;
    }

    /**
     * 将注册放在静态块中，那么静态块什么时候执行呢？
     * 1、当第一次创建某个类的新实例时
     * 2、当第一次调用某个类的任意静态方法时
     * 3、当第一次使用某个类或接口的任意非final静态字段时
     * 4、当第一次Class.forName时
     * 如果我们入StatusEnum创建枚举，那么在应用系统启动的过程中StatusEnum的静态块可能从未执行过，则枚举缓存注册失败，
     * 所有我们需要考虑延迟注册，代码如下：
     *
     * Class.forName(clazz.getName())被执行的两个必备条件：
     * 1、缓存中没有枚举class的键，也就是说没有执行过枚举向缓存注册的调用，见EnumCache.find方法对executeEnumStatic方法的调用；
     * 2、executeEnumStatic中的LOADED.put(clazz, true);还没有被执行过，也就是Class.forName(clazz.getName());没有被执行过；
     * 我们看到executeEnumStatic中用到了双重检查锁，所以分析一下正常情况下代码执行情况和性能：
     * 1、当静态块还未执行时，大量的并发执行find查询。
     * 此时executeEnumStatic中synchronized会阻塞其他线程；
     *
     * 第一个拿到锁的线程会执行Class.forName(clazz.getName());同时触发枚举静态块的同步执行；
     *
     * 之后其他线程会逐一拿到锁，第二次检查会不成立跳出executeEnumStatic；
     *
     * 2、当静态块已经执行，且静态块里面正常执行了缓存注册，大量的并发执行find查询。
     * executeEnumStatic方法不会调用，没有synchronized引发的排队问题；
     *
     * 3、当静态块已经执行，但是静态块里面没有调用缓存注册，大量的并发执行find查询。
     * find方法会调用executeEnumStatic方法，但是executeEnumStatic的第一次检查通不过；
     *
     * find方法会提示异常需要在静态块中添加注册缓存的代码；
     *
     * 总结：第一种场景下会有短暂的串行，但是这种内存计算短暂串行相比应用系统的业务逻辑执行是微不足道的，也就是说这种短暂的串行不会成为系统的性能瓶颈
     *
     * @param clazz
     * @param <E>
     */
    private static <E extends Enum> void executeEnumStatic(Class<E> clazz) {
        if (!LOADED.containsKey(clazz)) {
            synchronized (clazz) {
                if (!LOADED.containsKey(clazz)) {
                    try {
                        // 目的是让枚举类的static块运行，static块没有执行完是会阻塞在此的
                        Class.forName(clazz.getName());
                        LOADED.put(clazz, true);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    /**
     * 枚举缓存映射器函数式接口
     */
    @FunctionalInterface
    public interface EnumMapping<E extends Enum> {
        /**
         * 自定义映射器
         *
         * @param e 枚举
         * @return 映射关系，最终体现到缓存中
         */
        Object value(E e);
    }

}
