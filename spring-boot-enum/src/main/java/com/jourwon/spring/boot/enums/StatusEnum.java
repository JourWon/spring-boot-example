package com.jourwon.spring.boot.enums;

import com.jourwon.spring.boot.cache.EnumCache;

/**
 * 状态类枚举
 * 开闭原则：对修改是封闭的，对新增扩展是开放的。
 * 为了满足开闭原则，这里设计成有枚举主动注册到缓存，而不是有缓存主动加载枚举，这样设计的好处就是：当增加一个枚举时只需要在当前枚举的静态块中自主注册即可，不需要修改其他的代码
 *
 * @author JourWon
 * @date 2023/7/5
 */
public enum StatusEnum {

    /**
     * 初始化
     */
    INIT("I", "初始化"),

    /**
     * 处理中
     */
    PROCESSING("P", "处理中"),

    /**
     * 成功
     */
    SUCCESS("S", "成功"),

    /**
     * 失败
     */
    FAIL("F", "失败");

    /**
     * 编码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    StatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    static {
        // 通过名称构建缓存,通过EnumCache.findByName(StatusEnum.class,"SUCCESS",null);调用能获取枚举
        EnumCache.registerByName(StatusEnum.class, StatusEnum.values());
        // 通过code构建缓存,通过EnumCache.findByValue(StatusEnum.class,"S",null);调用能获取枚举
        EnumCache.registerByValue(StatusEnum.class, StatusEnum.values(), StatusEnum::getCode);
    }

}
