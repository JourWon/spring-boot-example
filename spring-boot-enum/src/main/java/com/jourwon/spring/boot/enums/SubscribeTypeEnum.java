package com.jourwon.spring.boot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

@Getter
@AllArgsConstructor
public enum SubscribeTypeEnum {
    /**
     * 人脸信息
     */
    FACE_INFO(1, "人脸信息", (c) -> {
        switch (c) {
            case TYPE1:
                return "TYPE1_FACE";
            case TYPE2:
                return "TYPE2_FACE";
            default:
                return "";
        }
    }),

    /**
     * 车辆信息
     */
    VEHICLE_INFO(2, "车辆信息", (c) -> {
        switch (c) {
            case TYPE1:
                return "TYPE1_VEHICLE";
            case TYPE2:
                return "TYPE2_VEHICLE";
            default:
                return "";
        }
    });

    /**
     * 编码
     */
    private final int code;

    /**
     * 描述
     */
    private final String message;

    /**
     * 主题
     */
    private final Function<PlatformTypeEnum, String> topic;

    /**
     * 根据编码获取枚举
     *
     * @param code 编码
     * @return SubscribeTypeEnum 枚举
     */
    public static SubscribeTypeEnum getByCode(Integer code) {
        return Arrays.stream(values()).filter(item -> Objects.equals(item.getCode(), code)).findAny().orElse(null);
    }

    /**
     * 根据订阅类型和平台类型获取对应的主题
     *
     * @param subscribeTypeEnum 订阅类型
     * @param platformTypeEnum  平台类型
     * @return String 主题
     */
    public static String getTopic(SubscribeTypeEnum subscribeTypeEnum, PlatformTypeEnum platformTypeEnum) {
        return subscribeTypeEnum.getTopic().apply(platformTypeEnum);
    }

}
