package com.jourwon.spring.boot.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jourwon.spring.boot.enums.*;
import com.jourwon.spring.boot.model.dto.FieldInfo;
import com.jourwon.spring.boot.util.FieldInfoUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author JourWon
 * @date 2022/6/1
 */
@Slf4j
public class EnumTest {

    public static void main(String[] args) throws JsonProcessingException {
        List<FieldInfo> faceInfoField = FieldInfoUtils.getFieldInfo(FaceInfoFieldEnum.values());
        log.info("人脸字段枚举:{}", new ObjectMapper().writeValueAsString(faceInfoField));

        log.info("========");
        List<FieldInfo> personInfoField = FieldInfoUtils.getFieldInfo(PersonInfoFieldEnum.values());
        log.info("人体字段枚举:{}", new ObjectMapper().writeValueAsString(personInfoField));

        log.info("========");
        System.out.println(MobilePhoneBrandEnum.getByCode("Huawei"));
        System.out.println(MobilePhoneBrandEnum.isValidBrandCode("Vivo"));


        System.out.println(SubscribeTypeEnum.getTopic(SubscribeTypeEnum.FACE_INFO, PlatformTypeEnum.TYPE2));
    }

}
