package com.jourwon.spring.boot.prop;

import lombok.Data;
import org.pf4j.RuntimeMode;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;

/**
 * 插件属性
 *
 * @author JourWon
 * @date 2022/3/20
 */
@Data
@ConfigurationProperties(prefix = "pf4j")
public class Pf4jProperties {

    /**
     * 运行模式,默认dev
     */
    private String mode = RuntimeMode.DEPLOYMENT.toString();

    /**
     * 插件目录
     */
    private String pluginsDir = defaultPluginsDir();

    private String defaultPluginsDir() {
        String userDir = System.getProperty("user.dir");
        return (new File(userDir)).getParent() + File.separator + "plugins" + File.separator;
    }

}
