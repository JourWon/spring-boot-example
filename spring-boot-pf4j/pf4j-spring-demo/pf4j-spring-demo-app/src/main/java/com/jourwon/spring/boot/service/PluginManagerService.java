package com.jourwon.spring.boot.service;

import com.jourwon.spring.boot.model.vo.PluginInfoVO;
import org.pf4j.PluginState;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 插件管理接口
 *
 * @author JourWon
 * @date 2022/3/24
 */
public interface PluginManagerService {

    /**
     * 根据插件id获取插件信息
     *
     * @param pluginId
     * @return
     */
    PluginInfoVO getPluginInfoVO(String pluginId);

    /**
     * 获取插件列表
     *
     * @return
     */
    List<PluginInfoVO> listPluginInfoVO();

    /**
     * 上传并加载插件
     *
     * @param file
     * @return
     */
    PluginInfoVO uploadAndLoadPlugin(MultipartFile file);

    /**
     * 启用插件
     *
     * @param pluginId
     * @return
     */
    PluginState startPlugin(String pluginId);

    /**
     * 停用插件
     *
     * @param pluginId
     * @return
     */
    PluginState stopPlugin(String pluginId);

    /**
     * 加载插件
     *
     * @param pluginPath
     * @return
     */
    PluginInfoVO loadPlugin(String pluginPath);

    /**
     * 删除插件
     *
     * @param pluginId
     * @return
     */
    boolean deletePlugin(String pluginId);

}
