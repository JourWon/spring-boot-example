package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.enums.CommonResponseCodeEnum;
import com.jourwon.spring.boot.model.PluginIdRequest;
import com.jourwon.spring.boot.model.vo.CommonResponse;
import com.jourwon.spring.boot.model.vo.PluginInfoVO;
import com.jourwon.spring.boot.service.PluginManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.pf4j.PluginState;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 插件管理接口
 *
 * @author JourWon
 * @date 2021/9/13
 */
@Slf4j
@RestController
@RequestMapping("/plugin")
@Api(tags = "插件管理接口")
public class PluginManagerController {

    @Resource
    private PluginManagerService pluginManagerService;

    @GetMapping
    @ApiOperation("根据插件id获取插件信息")
    public CommonResponse<PluginInfoVO> getPluginInfo(@RequestParam String pluginId) {
        PluginInfoVO pluginInfoVO = pluginManagerService.getPluginInfoVO(pluginId);
        return CommonResponse.success(pluginInfoVO);
    }

    @GetMapping("/list")
    @ApiOperation("获取插件列表")
    public CommonResponse<List<PluginInfoVO>> listPluginInfo() {
        List<PluginInfoVO> list = pluginManagerService.listPluginInfoVO();
        return CommonResponse.success(list);
    }

    @PostMapping(value = "/uploadAndLoadPlugin")
    @ApiOperation("上传并加载插件")
    @ApiImplicitParams({@ApiImplicitParam(name = "file", value = "文件流对象", required = true, dataType = "MultipartFile")})
    public PluginInfoVO uploadAndLoadPlugin(MultipartFile file) {
        return pluginManagerService.uploadAndLoadPlugin(file);
    }

    @PutMapping("/start")
    @ApiOperation("启用插件,插件需要加载之后才能启用")
    public CommonResponse startPlugin(@RequestBody PluginIdRequest request) {
        PluginState pluginState = pluginManagerService.startPlugin(request.getPluginId());
        if (pluginState.equals(PluginState.STARTED)) {
            return CommonResponse.success(true);
        } else {
            return CommonResponse.failure(CommonResponseCodeEnum.SYSTEM_EXCEPTION);
        }
    }

    @PutMapping("/stop")
    @ApiOperation("停用插件,不会删除插件,再次使用需要重新启用插件")
    public CommonResponse stopPlugin(@RequestBody PluginIdRequest request) {
        PluginState pluginState = pluginManagerService.stopPlugin(request.getPluginId());
        if (pluginState.equals(PluginState.STOPPED)) {
            return CommonResponse.success(true);
        } else {
            return CommonResponse.failure(CommonResponseCodeEnum.SYSTEM_EXCEPTION);
        }
    }

    @DeleteMapping("/delete")
    @ApiOperation("删除插件,会删除插件zip文件和解析后的文件夹,删除后不能再加载插件")
    public CommonResponse deletePlugin(@RequestBody PluginIdRequest request) {
        boolean result = pluginManagerService.deletePlugin(request.getPluginId());
        if (result) {
            return CommonResponse.success(true);
        } else {
            return CommonResponse.failure(CommonResponseCodeEnum.SYSTEM_EXCEPTION);
        }
    }

}
