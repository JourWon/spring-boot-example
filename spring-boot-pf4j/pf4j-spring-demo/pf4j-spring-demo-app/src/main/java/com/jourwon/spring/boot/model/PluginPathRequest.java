package com.jourwon.spring.boot.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author JourWon
 * @date 2022/3/20
 */
@Data
@ApiModel("插件路径请求体")
public class PluginPathRequest {

    @ApiModelProperty("插件路径")
    @NotBlank(message = "插件路径不能为空")
    private String pluginPath;

}
