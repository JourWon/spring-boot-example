package com.jourwon.spring.boot.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jourwon.spring.boot.enums.CommonResponseCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 统一响应对象
 *
 * @author JourWon
 * @date 2021/1/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CommonResponse-统一响应对象")
public class CommonResponse<T> implements Serializable {

    private static final long serialVersionUID = -1338376281028943181L;

    @ApiModelProperty(value = "响应编码")
    private String code;

    @ApiModelProperty(value = "响应信息")
    private String message;

    @ApiModelProperty(value = "业务数据")
    private T data;

    @ApiModelProperty(value = "响应日期时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime localDateTime = LocalDateTime.now();

    public CommonResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public CommonResponse(CommonResponseCodeEnum commonResponseCodeEnum) {
        this.code = commonResponseCodeEnum.getCode();
        this.message = commonResponseCodeEnum.getMessage();
    }



    public CommonResponse(CommonResponseCodeEnum commonResponseCodeEnum, T data) {
        this.code = commonResponseCodeEnum.getCode();
        this.message = commonResponseCodeEnum.getMessage();
        this.data = data;
    }

    public static <T> CommonResponse<T> success() {
        return new CommonResponse<>(CommonResponseCodeEnum.SUCCESS);
    }

    public static <T> CommonResponse<T> success(T data) {
        return new CommonResponse<>(CommonResponseCodeEnum.SUCCESS, data);
    }

    public static <T> CommonResponse<T> success(CommonResponseCodeEnum commonResponseCodeEnum, T data) {
        return new CommonResponse<>(commonResponseCodeEnum, data);
    }

    public static <T> CommonResponse<T> failure() {
        return new CommonResponse<>(CommonResponseCodeEnum.SYSTEM_EXCEPTION);
    }

    public static <T> CommonResponse<T> failure(CommonResponseCodeEnum commonResponseCodeEnum) {
        return new CommonResponse<>(commonResponseCodeEnum);
    }

}
