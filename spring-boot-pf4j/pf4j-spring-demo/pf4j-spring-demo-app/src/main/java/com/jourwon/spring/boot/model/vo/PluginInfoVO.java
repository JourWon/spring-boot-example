package com.jourwon.spring.boot.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.pf4j.Plugin;
import org.pf4j.PluginState;
import org.pf4j.PluginWrapper;
import org.springframework.beans.BeanUtils;

/**
 * 插件信息
 *
 * @author JourWon
 * @date 2022/3/24
 */
@Data
@AllArgsConstructor
@ApiModel("插件信息")
public class PluginInfoVO {

    @ApiModelProperty("插件id")
    private String pluginId;

    @ApiModelProperty("插件描述")
    private String pluginDescription;

    @ApiModelProperty("插件Class")
    private String pluginClass;

    @ApiModelProperty("插件版本")
    private String version;

    @ApiModelProperty("插件提供者")
    private String provider;

    @ApiModelProperty("插件状态")
    private PluginState pluginState;

    public PluginInfoVO() {
        this.pluginClass = Plugin.class.getName();
    }

    public static PluginInfoVO toPluginInfoVO(PluginWrapper pluginWrapper) {
        if (pluginWrapper == null) {
            return null;
        }
        PluginInfoVO pluginInfoVO = new PluginInfoVO();
        BeanUtils.copyProperties(pluginWrapper.getDescriptor(), pluginInfoVO);
        pluginInfoVO.setPluginState(pluginWrapper.getPluginState());
        return pluginInfoVO;
    }

}
