package com.jourwon.spring.boot.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 分页查询请求对象
 *
 * @author JourWon
 * @date 2022/10/30
 */
@Data
@ApiModel("QueryPageInfo-分页查询请求对象")
public class QueryPageInfo<T> implements Serializable {

    private static final long serialVersionUID = 2952179312728757298L;

    /**
     * 页码，分页查询时必填，从1开始
     */
    @NotNull(message = "页码不能为空")
    @Min(value = 1, message = "页码不能小于1")
    @ApiModelProperty(value = "页码，分页查询时必填，从1开始", required = true)
    private Integer pageNo;

    /**
     * 页大小，分页查询时必填；
     * 若要求按最多数量返回结果，则PageNo指定为1，
     * PageSize 指定需要返回的结果数量（如最多返回40 条，则PageSize 指定为40）
     */
    @NotNull(message = "页大小不能为空")
    @Range(min = 1, max = 100, message = "页大小必须在1-100的整形之间")
    @ApiModelProperty(value = "页大小，分页查询时必填", required = true)
    private Integer pageSize;

    /**
     * 查询条件对象，对象内的数据为具体某一查询对象的属性键值对
     */
    @ApiModelProperty(value = "查询条件对象")
    private T condition;

}
