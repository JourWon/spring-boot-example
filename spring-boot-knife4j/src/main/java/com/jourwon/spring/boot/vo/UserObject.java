package com.jourwon.spring.boot.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户列表对象
 *
 * @author JourWon
 * @date 2022/10/30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "UserObject-用户列表对象")
public class UserObject {

    private List<UserVO> userObject;

}
