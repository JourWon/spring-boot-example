package com.jourwon.spring.boot.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Elasticsearch 文档
 *
 * @author JourWon
 * @date 2021/12/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EsDoc<T> {

    private String id;

    private T data;

}
