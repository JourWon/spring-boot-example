package com.jourwon.spring.boot.respsonse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 分页查询结果对象
 *
 * @author JourWon
 * @date 2021/1/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("ResponsePageInfo-分页查询结果对象")
public class ResponsePageInfo<T> implements Serializable {

    @ApiModelProperty(value = "页码，分页查询时必填，从1开始", required = true)
    private Integer pageNo;

    @ApiModelProperty(value = "页大小，分页查询时必填", required = true)
    private Integer pageSize;

    @ApiModelProperty("当前页码记录数")
    private Integer currentPageSize;

    @ApiModelProperty("总页数")
    private Integer totalPages;

    @ApiModelProperty("符合条件的结果总数")
    private Long totalElements;

    @ApiModelProperty("具体结果数据")
    private List<T> content;

    /**
     * 空分页数据
     *
     * @param <T> 空
     * @return ResponsePageInfo<T> 分页数据结构
     */
    public static <T> ResponsePageInfo<T> emptyPage(Integer pageNum, Integer pageSize) {
        return new ResponsePageInfo<>(pageNum, pageSize, 0, 0, 0L, Collections.emptyList());
    }

    public static <T> ResponsePageInfo<T> ofPage(Integer pageNo, Integer pageSize, Long totalElements, List<T> content) {
        ResponsePageInfo<T> responsePageInfo = emptyPage(pageNo, pageSize);
        responsePageInfo.setTotalElements(totalElements);
        responsePageInfo.setContent(content);

        Integer totalPages = (int) (totalElements + pageSize - 1) / pageSize;
        responsePageInfo.setTotalPages(totalPages);

        return responsePageInfo;
    }

}
