package com.jourwon.spring.boot.dto;

import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * Elasticsearch scroll查询结果
 *
 * @author JourWon
 * @date 2023/4/27
 */
@Data
public final class EsScrollResult<T> {

    /**
     * 是否成功，true表示成功，false表示失败
     */
    private boolean success;

    /**
     * 当前查询的scrollId
     */
    private String currentScrollId;

    /**
     * 下一次查询的scrollId
     */
    private String nextScrollId;

    /**
     * 结果
     */
    private List<T> list;

    public static <T> EsScrollResult<T> ofSuccess(String currentScrollId, String nextScrollId, List<T> list) {
        EsScrollResult<T> esScrollResult = new EsScrollResult<>();

        esScrollResult.setCurrentScrollId(currentScrollId);
        esScrollResult.setNextScrollId(nextScrollId);
        esScrollResult.setList(list);
        esScrollResult.setSuccess(true);

        return esScrollResult;
    }

    public static <T> EsScrollResult<T> ofEmpty(String currentScrollId) {
        EsScrollResult<T> esScrollResult = new EsScrollResult<>();

        esScrollResult.setCurrentScrollId(currentScrollId);
        esScrollResult.setNextScrollId(null);
        esScrollResult.setList(Collections.emptyList());
        esScrollResult.setSuccess(false);

        return esScrollResult;
    }

}
