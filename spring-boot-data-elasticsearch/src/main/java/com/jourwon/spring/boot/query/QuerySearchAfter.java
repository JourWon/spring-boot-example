package com.jourwon.spring.boot.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * SearchAfter请求参数
 *
 * @author JourWon
 * @date 2024/3/11
 */
@Getter
@Setter
@ApiModel(value = "SearchAfter请求参数")
public class QuerySearchAfter {

    @ApiModelProperty(value = "每一页的排序ID记录,key=页码,value=sortId。第1页为空,每次请求追加上一次的sortId到这个Map,后端原样返回")
    private Map<String, String> lastSortIdMap;

    @ApiModelProperty(value = "排序ID,第一次请求时不需要,但是下一次请求时必须从响应结果里面获取到再请求")
    private String sortId;

    @ApiModelProperty(value = "查询记录数", required = true, example = "10")
    @NotNull(message = "size不能为空")
    @Min(value = 1, message = "size必须为大于1的正整数")
    private Integer size;

    @Valid
    @NotNull(message = "排序字段列表不能为空")
    @Size(min = 1, message = "至少指定一个排序字段")
    @ApiModelProperty(value = "排序字段列表", required = true)
    private List<Sort> sortList;

    @ApiModelProperty(value = "是否返回总记录数,true-是;false-否(默认)", required = true, example = "false")
    private boolean trackTotalHits = false;

    @Getter
    @Setter
    public static class Sort {
        @NotBlank(message = "排序字段不能为空")
        @ApiModelProperty(value = "排序字段", example = "CREATE_TIME")
        private String sortField;

        @NotBlank(message = "排序顺序不能为空")
        @Pattern(regexp = "ASC|DESC", message = "排序顺序:顺序ASC,倒序DESC")
        @ApiModelProperty(value = "排序顺序:顺序ASC,倒序DESC", required = true, example = "DESC")
        private String sortOrder;
    }

}
