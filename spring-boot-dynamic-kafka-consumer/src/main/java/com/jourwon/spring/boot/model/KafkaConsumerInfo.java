package com.jourwon.spring.boot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * kafka消费者信息
 *
 * @author JourWon
 * @date 2022/3/21
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KafkaConsumerInfo {

    /**
     * 消费者id
     */
    private String consumerId;

    /**
     * 组id
     */
    private String groupId;

    /**
     * 监听器id
     */
    private String listenerId;

    /**
     * 是否运行
     */
    private Boolean running;

    /**
     * kafka消费者主题分区信息列表
     */
    private List<KafkaConsumerTopicPartitionInfo> topicPartitions;

}
