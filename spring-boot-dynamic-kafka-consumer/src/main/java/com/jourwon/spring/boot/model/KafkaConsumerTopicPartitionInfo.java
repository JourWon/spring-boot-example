package com.jourwon.spring.boot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

/**
 * kafka消费者主题分区信息
 *
 * @author JourWon
 * @date 2022/3/21
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KafkaConsumerTopicPartitionInfo {

    /**
     * 主题
     */
    private String topic;

    /**
     * 分区数
     */
    private Integer partition;

}
