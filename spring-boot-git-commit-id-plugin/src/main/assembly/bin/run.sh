#!/bin/bash
#
# 启动脚本时,如果遇到"-bash: ./run.sh: /bin/bash^M: bad interpreter: No such file or directory"提示错误解决办法
#
# 第一种: vi/vim编辑模式下,输入 ":set ff=unix", 然后退出保存。
#
# 第二种: 命令行运行 sed -i 's/\r$//' run.sh
#
# 选择以上任意一种就可以把run.sh的fileformat从windows的dos转换成linux的unix, 推荐第二种方法,一步到位

cd `dirname $0`
#-------------------------------------------------------------------
# 定义变量
#-------------------------------------------------------------------

#当前路径
BIN_DIR=`pwd`

cd ..

#上一级目录路径
DEPLOY_DIR=`pwd`

#配置文件路径
CONF_DIR=${DEPLOY_DIR}/conf

# 模块名
MODEL_NAME="${project.artifactId}"

# 运行包名--如有变化,需要修改启动的JAR包名字
MODEL_JAR="${DEPLOY_DIR}/lib/${project.build.finalName}.jar"

# 运行参数
MODEL_VARS="--spring.config.location=${CONF_DIR}/ --logging.config=${CONF_DIR}/logback-spring.xml"

# JVM参数
JVM_VARS="-server -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -Xms4g -Xmx4g"

# 前台/后台: 0-前台， 1-后台
MODEL_DAEMON=1

#-------------------------------------------------------------------
# 以下内容请不要修改
#-------------------------------------------------------------------
SLEEP_MIN=5

# model info can be define here
MODEL_SYMBOL=${MODEL_NAME}
GREP_KEY="-Diname="${MODEL_SYMBOL}


#----------------------------------------------------------
# function print_usage
#----------------------------------------------------------
print_usage()
{
echo ""
echo "h|H|help|HELP             ---Print help information."
echo "start                     ---Start the ${MODEL_NAME} server."
echo "stop                      ---Stop the ${MODEL_NAME} server."
echo "restart                   ---Restart the ${MODEL_NAME} server."
echo "status                    ---Status the ${MODEL_NAME} server."
}

#-------------------------------------------------------------------
# function model_is_exist
#-------------------------------------------------------------------
model_is_exist()
{
localModelPid=`ps -ef | grep -w "${MODEL_JAR}" | grep -v grep | awk '{print $2}'`
if [ -z "${localModelPid}" ]
then
    return 1
else
    expr ${localModelPid} + 0 &> /dev/null

    if [ $? -ne 0 ]
    then
        localModelPid=`ps -ef | grep -w "${MODEL_JAR}" | grep -v grep | awk '{print $2}' | grep -Eo '[0-9]+'`
    fi

    echo "pid is ${localModelPid}"
    return 0
fi
}

#-------------------------------------------------------------------
# function model_start
#-------------------------------------------------------------------
model_start()
{
model_is_exist

if [ $? -eq "0" ]
then
    echo "${MODEL_NAME} is running yet. pid ${localModelPid}."
    return 0
else
    if [ $MODEL_DAEMON = 0 ]
    then
        echo "try to start ${MODEL_NAME} ... foreground"
        $JAVA_HOME/bin/java ${GREP_KEY} -jar ${JVM_VARS} ${MODEL_JAR} ${MODEL_VARS}
    else
        echo "try to start ${MODEL_NAME} ... backgroud"
        nohup $JAVA_HOME/bin/java ${GREP_KEY} -jar ${JVM_VARS} ${MODEL_JAR} ${MODEL_VARS} > /dev/null 2>&1 &
        sleep $SLEEP_MIN
        model_is_exist

        if [ $? -eq "0" ]
        then
            echo "${MODEL_NAME} is running. pid ${localModelPid}."
            return 0
        else
            echo "failed to start ${MODEL_NAME}! see the output log for more details."
            return 1
        fi
    fi
fi
}

#-------------------------------------------------------------------
# function model_stop
#-------------------------------------------------------------------
model_stop()
{
echo "try to stop ${MODEL_NAME} ..."
model_is_exist
count=0
if [ $? -eq 0 ]
then
    kill ${localModelPid}
    model_is_exist

    while [ $? -eq 0 ]
    do
        ((count++))
        echo "stopping ${MODEL_NAME} ${count} ..."
        sleep 1
        model_is_exist
    done

    echo "${MODEL_NAME} stopped."
    return 0
else
    echo "${MODEL_NAME} is not running!"
    return 1
fi
}

#-------------------------------------------------------------------
# function model_restart
#-------------------------------------------------------------------
model_restart()
{
model_stop

model_start
}

#-------------------------------------------------------------------
# function model_status
#-------------------------------------------------------------------
model_status()
{
    model_is_exist

    if [ $? -eq 0 ]
    then
        echo "${MODEL_NAME} is running. pid ${localModelPid}."
    else
        echo "${MODEL_NAME} is not running."
    fi
}

#-------------------------------------------------------------------
# function parse_para
#-------------------------------------------------------------------
parse_para()
{
case "$1" in
    start) model_start;;
    stop) model_stop;;
    status) model_status;;
    restart) model_restart;;
    *) echo "illegal parameter : $1";print_usage;;
esac
}

#-------------------------------------------------------------------
# main
#-------------------------------------------------------------------
parse_para "$1"

