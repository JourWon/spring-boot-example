package com.jourwon.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2022/3/25
 */
@SpringBootApplication
public class SpringBootFactoryMethodApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootFactoryMethodApplication.class, args);
    }

}
