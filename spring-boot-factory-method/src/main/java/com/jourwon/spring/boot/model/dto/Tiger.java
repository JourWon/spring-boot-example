package com.jourwon.spring.boot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 老虎
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tiger {

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 颜色
     */
    private String color;

}
