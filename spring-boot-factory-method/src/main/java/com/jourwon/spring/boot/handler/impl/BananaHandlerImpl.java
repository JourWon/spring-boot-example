package com.jourwon.spring.boot.handler.impl;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.enums.FruitTypeEnum;
import com.jourwon.spring.boot.handler.FruitHandler;
import com.jourwon.spring.boot.model.dto.Banana;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 香蕉处理器
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Slf4j
@Component
public class BananaHandlerImpl implements FruitHandler<Banana> {

    @Override
    public String getType() {
        return FruitTypeEnum.BANANA.getType();
    }

    @Override
    public void handle(Banana obj) {
        log.info("香蕉处理方法,香蕉对象:{}", JSON.toJSONString(obj));
    }

}
