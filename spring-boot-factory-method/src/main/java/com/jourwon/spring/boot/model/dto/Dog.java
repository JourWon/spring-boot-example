package com.jourwon.spring.boot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 小狗
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dog {

    /**
     * 品种
     */
    private String variety;

    /**
     * 名字
     */
    private String name;

}
