package com.jourwon.spring.boot.handler;

import org.springframework.beans.factory.InitializingBean;

/**
 * 处理器工厂
 *
 * @author JourWon
 * @date 2022/3/24
 */
public interface HandlerFactory<T> extends InitializingBean {

    /**
     * 获取处理器
     *
     * @param type
     * @return
     */
    T getHandler(String type);

}
