package com.jourwon.spring.boot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 苹果
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Apple {

    /**
     * 价格
     */
    private Float price;

    /**
     * 颜色
     */
    private String color;

}
