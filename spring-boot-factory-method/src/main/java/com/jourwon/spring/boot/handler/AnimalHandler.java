package com.jourwon.spring.boot.handler;

/**
 * 动物类型处理器
 *
 * @author JourWon
 * @date 2022/3/25
 */
public interface AnimalHandler<T> {

    /**
     * 数据类型
     *
     * @return
     */
    String getType();

    /**
     * 处理方法
     *
     * @param obj
     */
    void handle(T obj);

}
