package com.jourwon.spring.boot.handler.impl;

import com.jourwon.spring.boot.handler.AnimalHandler;
import com.jourwon.spring.boot.handler.HandlerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 动物处理器工厂
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Slf4j
@Component
public class AnimalHandlerFactoryImpl implements HandlerFactory<AnimalHandler> {

    @Resource
    private List<AnimalHandler> animalHandlerList;

    private Map<String, AnimalHandler> animalHandlerMap = new ConcurrentHashMap<>();

    @Override
    public void afterPropertiesSet() {
        animalHandlerMap = animalHandlerList.stream().collect(Collectors.toMap(AnimalHandler::getType, Function.identity()));
    }

    @Override
    public AnimalHandler getHandler(String type) {
        if (CollectionUtils.isEmpty(animalHandlerMap)) {
            log.warn("动物处理器列表为空");
            return null;
        }

        return animalHandlerMap.getOrDefault(type, null);
    }

}
