package com.jourwon.spring.boot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 动物类型枚举
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Getter
@AllArgsConstructor
public enum AnimalTypeEnum {

    /**
     * 小狗
     */
    DOG("DOG", "小狗"),

    /**
     * 老虎
     */
    TIGER("TIGER", "老虎"),
    ;

    /**
     * 类型
     */
    private String type;

    /**
     * 名称
     */
    private String name;

    public static AnimalTypeEnum fromValue(String alias) {
        for (AnimalTypeEnum value : values()) {
            if (value.type.equals(alias)) {
                return value;
            }
        }
        return null;
    }

}
