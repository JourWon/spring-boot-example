package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.enums.AnimalTypeEnum;
import com.jourwon.spring.boot.handler.AnimalHandler;
import com.jourwon.spring.boot.handler.HandlerFactory;
import com.jourwon.spring.boot.model.dto.Dog;
import com.jourwon.spring.boot.model.dto.Tiger;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 动物接口
 *
 * @author JourWon
 * @date 2022/3/25
 */
@RestController
@Api(tags = "动物接口")
@RequestMapping("animal")
public class AnimalHandlerController {

    @Resource
    private HandlerFactory<AnimalHandler> factory;

    @GetMapping("/dog")
    @ApiOperation("获取小狗信息")
    public void dog() {
        factory.getHandler(AnimalTypeEnum.DOG.getType()).handle(new Dog("雪橇犬", "阿拉斯加犬"));
    }

    @GetMapping("/tiger")
    @ApiOperation("获取老虎信息")
    public void tiger() {
        factory.getHandler(AnimalTypeEnum.TIGER.getType()).handle(new Tiger(2, "白色"));
    }

}
