package com.jourwon.spring.boot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 水果类型枚举
 *
 * @author JourWon
 * @date 2022/3/24
 */
@Getter
@AllArgsConstructor
public enum FruitTypeEnum {

    /**
     * 苹果
     */
    APPLE("APPLE", "苹果"),

    /**
     * 香蕉
     */
    BANANA("BANANA", "香蕉"),
    ;

    /**
     * 类型
     */
    private String type;

    /**
     * 名称
     */
    private String name;

    public static FruitTypeEnum fromValue(String alias) {
        for (FruitTypeEnum value : values()) {
            if (value.type.equals(alias)) {
                return value;
            }
        }
        return null;
    }

}
