package com.jourwon.spring.boot.handler.impl;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.enums.AnimalTypeEnum;
import com.jourwon.spring.boot.handler.AnimalHandler;
import com.jourwon.spring.boot.model.dto.Dog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 小狗处理器
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Slf4j
@Component
public class DogHandlerImpl implements AnimalHandler<Dog> {

    @Override
    public String getType() {
        return AnimalTypeEnum.DOG.getType();
    }

    @Override
    public void handle(Dog obj) {
        log.info("小狗处理方法,小狗对象:{}", JSON.toJSONString(obj));
    }

}
