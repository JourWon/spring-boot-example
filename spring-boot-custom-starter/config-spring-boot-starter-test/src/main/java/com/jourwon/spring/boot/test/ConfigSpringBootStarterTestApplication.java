package com.jourwon.spring.boot.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2021/12/23
 */
@SpringBootApplication
public class ConfigSpringBootStarterTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigSpringBootStarterTestApplication.class, args);
    }

}
