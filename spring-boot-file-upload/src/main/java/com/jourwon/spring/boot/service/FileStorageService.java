package com.jourwon.spring.boot.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * 文件上传接口
 *
 * @author JourWon
 * @date 2021/12/28
 */
public interface FileStorageService {

    /**
     * 初始化方法,创建文件夹
     */
    void init();

    /**
     * 保存文件
     *
     * @param multipartFile 文件
     */
    void save(MultipartFile multipartFile);

    /**
     * 根据文件名加载文件
     *
     * @param filename 文件名
     * @return 资源
     */
    Resource load(String filename);

    /**
     * 加载所有的文件
     *
     * @return 文件路径流
     */
    Stream<Path> load();

    /**
     * 递归删除文件
     */
    void clear();

}
