package com.jourwon.spring.boot.mapper;

import com.jourwon.spring.boot.enums.PaymentType;
import com.jourwon.spring.boot.enums.PaymentTypeView;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;
import org.mapstruct.factory.Mappers;

/**
 * @author JourWon
 * @date 2023/4/2
 */
@Mapper
public interface PaymentTypeMapper {

    PaymentTypeMapper INSTANCE = Mappers.getMapper(PaymentTypeMapper.class);

    // @ValueMappings({
    //         @ValueMapping(source = "CARD_VISA", target = "CARD"),
    //         @ValueMapping(source = "CARD_MASTER", target = "CARD"),
    //         @ValueMapping(source = "CARD_CREDIT", target = "CARD")
    // })
    // PaymentTypeView paymentTypeToPaymentTypeView(PaymentType paymentType);

    /**
     * 实体类转换方法
     *
     * @param paymentType paymentType
     * @return PaymentTypeView
     */
    @ValueMapping(source = MappingConstants.ANY_UNMAPPED, target = "CARD")
    PaymentTypeView paymentTypeToPaymentTypeView(PaymentType paymentType);

}
