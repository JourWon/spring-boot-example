package com.jourwon.spring.boot.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 实体类
 * 添加了@Builder注解则需要添加满参构造器
 * 否则报:无法将类 com.jourwon.spring.boot.entity.Doctor中的构造器 Doctor 应用到给定类型
 *
 * @author JourWon
 * @date 2023/4/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Doctor {

    /**
     * id
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 专业
     */
    private String specialty;

    /**
     * 患者列表
     */
    private List<Patient> patientList;

    private String externalId;

    private LocalDateTime availability;

}
