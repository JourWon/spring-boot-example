package com.jourwon.spring.boot.entity;

import lombok.*;

import java.util.List;

/**
 * @author JourWon
 * @date 2023/4/4
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoctorPatientSummary {

    private int doctorId;
    private int patientCount;
    private String doctorName;
    private String specialization;
    private String institute;
    private List<Integer> patientIds;

}
