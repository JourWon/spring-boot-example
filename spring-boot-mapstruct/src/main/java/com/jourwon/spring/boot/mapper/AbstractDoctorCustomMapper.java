package com.jourwon.spring.boot.mapper;

import com.jourwon.spring.boot.dto.DoctorDto;
import com.jourwon.spring.boot.entity.Doctor;
import com.jourwon.spring.boot.entity.DoctorPatientSummary;
import com.jourwon.spring.boot.entity.Education;
import com.jourwon.spring.boot.entity.Patient;
import org.mapstruct.*;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author JourWon
 * @date 2023/4/4
 */
@Mapper(uses = {PatientMapper.class}, componentModel = "spring")
public abstract class AbstractDoctorCustomMapper {

    public DoctorPatientSummary toDoctorPatientSummary(Doctor doctor, Education education) {
        return DoctorPatientSummary.builder()
                .doctorId(doctor.getId())
                .doctorName(doctor.getName())
                .patientCount(doctor.getPatientList().size())
                .patientIds(doctor.getPatientList()
                        .stream()
                        .map(Patient::getId)
                        .collect(Collectors.toList()))
                .institute(education.getInstitute())
                .specialization(education.getDegreeName())
                .build();
    }

    @BeforeMapping
    protected void validate(Doctor doctor) {
        if (doctor.getPatientList() == null) {
            doctor.setPatientList(new ArrayList<>());
        }
    }

    @AfterMapping
    protected void updateResult(@MappingTarget DoctorDto doctorDto) {
        doctorDto.setName(doctorDto.getName().toUpperCase());
        doctorDto.setDegree(doctorDto.getDegree().toUpperCase());
        doctorDto.setSpecialization(doctorDto.getSpecialization().toUpperCase());
    }

    /**
     * 实体类转换方法
     *
     * @param doctor doctor
     * @return DoctorDto
     */
    @Mapping(source = "doctor.patientList", target = "patientDtoList")
    @Mapping(source = "doctor.specialty", target = "specialization")
    public abstract DoctorDto toDoctorDto(Doctor doctor);

    /**
     * 实体类更新方法
     *
     * @param doctor    doctor
     * @param doctorDto doctorDto
     */
    @InheritConfiguration
    public abstract void updateModel(Doctor doctor, @MappingTarget DoctorDto doctorDto);

}
