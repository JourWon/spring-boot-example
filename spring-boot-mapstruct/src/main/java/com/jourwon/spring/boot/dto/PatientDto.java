package com.jourwon.spring.boot.dto;

import lombok.*;

import java.time.LocalDate;

/**
 * @author JourWon
 * @date 2023/4/2
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PatientDto {

    /**
     * id
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    private LocalDate dateOfBirth;

}
