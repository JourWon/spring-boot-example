package com.jourwon.spring.boot.enums;

/**
 * @author JourWon
 * @date 2023/4/2
 */
public enum PaymentTypeView {

    /**
     * 现金
     */
    CASH,

    /**
     * 支票
     */
    CHEQUE,

    /**
     * 信用卡
     */
    CARD

}
