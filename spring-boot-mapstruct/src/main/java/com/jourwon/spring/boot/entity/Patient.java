package com.jourwon.spring.boot.entity;

import lombok.*;

/**
 * @author JourWon
 * @date 2023/4/2
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Patient {

    /**
     * id
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    private String dateOfBirth;

}
