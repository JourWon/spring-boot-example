package com.jourwon.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2023/4/3
 */
@SpringBootApplication
public class SpringBootMapstructApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMapstructApplication.class, args);
    }

}
