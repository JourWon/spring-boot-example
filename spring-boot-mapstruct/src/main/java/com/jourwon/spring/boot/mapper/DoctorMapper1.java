// package com.jourwon.spring.boot.mapper;
//
// import com.jourwon.spring.boot.dto.DoctorDto;
// import com.jourwon.spring.boot.entity.Doctor;
// import com.jourwon.spring.boot.entity.Education;
// import org.mapstruct.*;
// import org.mapstruct.factory.Mappers;
//
// import java.util.List;
//
// /**
//  * Doctor映射器接口
//  *
//  * @author JourWon
//  * @date 2023/4/2
//  */
// @Mapper(uses = {PatientMapper.class})
// public interface DoctorMapper1 {
//
//     DoctorMapper1 INSTANCE = Mappers.getMapper(DoctorMapper1.class);
//
//     /**
//      * doctor转换为DoctorDto
//      *
//      * @param doctor doctor
//      * @return DoctorDto UserDto
//      */
//     @Named(value = "doctorToDoctorDto")
//     DoctorDto doctorToDoctorDto(Doctor doctor);
//
//     /**
//      * doctor转换为DoctorDto
//      * 使用 @Mapping 注解，设置 source 和 target 映射不同属性名称的的两个字段。
//      *
//      * @param doctor doctor
//      * @return DoctorDto DoctorDto
//      */
//     @Mapping(source = "specialty", target = "specialization")
//     DoctorDto toDto(Doctor doctor);
//
//     /**
//      * 多个源类转换
//      *
//      * @param doctor    doctor
//      * @param education education
//      * @return DoctorDto DoctorDto
//      */
//     @Mapping(source = "doctor.specialty", target = "specialization")
//     @Mapping(source = "education.degreeName", target = "degree")
//     DoctorDto toDtoWithMultiSource(Doctor doctor, Education education);
//
//     @Mapping(source = "doctor.patientList", target = "patientDtoList")
//     @Mapping(source = "doctor.specialty", target = "specialization")
//     DoctorDto toDtoWithSubObject(Doctor doctor);
//
//     @Mapping(source = "doctorDto.patientDtoList", target = "patientList")
//     @Mapping(source = "doctorDto.specialization", target = "specialty")
//     void updateModel(DoctorDto doctorDto, @MappingTarget Doctor doctor);
//
//     /**
//      * 当遇到多个doctor转换为doctorDto的方法时，会报错Ambiguous mapping methods found for mapping collection element
//      * 需要指定一个转换方法
//      *
//      * @param doctor doctor
//      * @return List<DoctorDto>
//      */
//     @IterableMapping(qualifiedByName = "doctorToDoctorDto")
//     List<DoctorDto> map(List<Doctor> doctor);
//
// }
