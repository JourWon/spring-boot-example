package com.jourwon.spring.boot.mapper;

import com.jourwon.spring.boot.dto.EducationDto;
import com.jourwon.spring.boot.entity.Education;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author JourWon
 * @date 2023/4/3
 */
@Mapper
public interface EducationMapper {

    EducationMapper INSTANCE = Mappers.getMapper(EducationMapper.class);

    /**
     * 实体类转换方法
     *
     * @param educationList educationList
     * @return List<EducationDto>
     */
    List<EducationDto> map(List<Education> educationList);

}
