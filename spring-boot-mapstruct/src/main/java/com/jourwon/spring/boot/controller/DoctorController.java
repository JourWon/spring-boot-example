package com.jourwon.spring.boot.controller;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.dto.DoctorDto;
import com.jourwon.spring.boot.entity.Doctor;
import com.jourwon.spring.boot.mapper.DoctorMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author JourWon
 * @date 2023/4/2
 */
@Api(tags = "医生")
@RestController
@RequestMapping("/doctor")
public class DoctorController {

    @Resource
    private DoctorMapper doctorMapper;

    @GetMapping("/mapConvert")
    @ApiOperation("mapstruct整合spring测试")
    public String mapConvert() {
        Map<String, Doctor> map = new HashMap<String, Doctor>(8) {{
            put("1", Doctor.builder().id(1).name("Rocket").build());
            put("2", Doctor.builder().id(2).name("Mess").build());
        }};

        Map<String, DoctorDto> dtoMap = doctorMapper.mapConvert(map);
        String result = JSON.toJSONString(dtoMap);
        return result;
    }

}
