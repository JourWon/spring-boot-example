package com.jourwon.spring.boot.entity;

import lombok.*;

/**
 * 教育
 *
 * @author JourWon
 * @date 2023/4/2
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Education {

    /**
     * 学历
     */
    private String degreeName;

    /**
     * 学院
     */
    private String institute;

}
