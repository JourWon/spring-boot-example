package com.jourwon.spring.boot.mapper;

import com.jourwon.spring.boot.dto.HospitalDto;
import com.jourwon.spring.boot.entity.Hospital;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author JourWon
 * @date 2023/4/3
 */
@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, uses = {DoctorMapper.class})
public interface HospitalMapper {

    HospitalMapper INSTANCE = Mappers.getMapper(HospitalMapper.class);

    /**
     * 实体类转换方法
     *
     * @param hospital hospital
     * @return HospitalDto
     */
    @Mapping(source = "hospital.doctors", target = "doctors", qualifiedByName = "toDto")
    HospitalDto toDto(Hospital hospital);

}
