package com.jourwon.spring.boot.enums;

/**
 * @author JourWon
 * @date 2023/4/2
 */
public enum PaymentType {

    /**
     * 现金
     */
    CASH,

    /**
     * 支票
     */
    CHEQUE,

    /**
     * VISA信用卡
     */
    CARD_VISA,

    /**
     * MASTER信用卡
     */
    CARD_MASTER,

    /**
     * 信用卡
     */
    CARD_CREDIT

}
