package com.jourwon.spring.boot.entity;

import javax.validation.ValidationException;

/**
 * @author JourWon
 * @date 2023/4/4
 */
public class Validator {

    public int validateId(Integer id) throws ValidationException {
        if (id == -1) {
            throw new ValidationException("Invalid value in ID");
        }
        return id;
    }

}
