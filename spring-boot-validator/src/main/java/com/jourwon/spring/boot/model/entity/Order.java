package com.jourwon.spring.boot.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author JourWon
 * @date 2022/1/20
 */
@Data
@ApiModel("订单")
public class Order {

    @NotNull(message = "订单id不能为空")
    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @NotBlank(message = "商品名称不能为空")
    @ApiModelProperty(value = "商品名称")
    private String productName;

}
