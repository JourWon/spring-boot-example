package com.jourwon.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2022/3/17
 */
@SpringBootApplication
public class SpringBootMybatisPlusDynamicJdbctemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybatisPlusDynamicJdbctemplateApplication.class, args);
    }

}
