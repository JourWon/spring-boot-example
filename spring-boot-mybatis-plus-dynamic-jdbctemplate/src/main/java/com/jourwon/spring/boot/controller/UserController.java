package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.model.vo.UserVO;
import com.jourwon.spring.boot.service.UserService;
import com.jourwon.spring.boot.util.SqlBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 用户表 前端控制器
 *
 * @author JourWon
 * @date 2021-02-10
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/list/master")
    @ApiOperation("查询所有的用户列表(主数据源)")
    public List<UserVO> listMaster() {
        return userService.listMaster();
    }

    @GetMapping("/list/slave")
    @ApiOperation("查询所有的用户列表(丛数据源)")
    public List<UserVO> listSlave() {
        return userService.listSlave();
    }

    @GetMapping("/sqlBuilderTest")
    @ApiOperation("SqlBuilder测试")
    public String sqlBuilderTest() {
        String sql = SqlBuilder.selectBuilder().selectFrom("CODE", Arrays.asList("AGE", "NAME")).build()
                .where().equal("NAME", "张三").and()
                .between("AGE", 3, 20).and()
                .great("HEIGHT", 20).and()
                .greatEq("WEIGHT", 50).and()
                .less("PA", 4).and()
                .lessEq("BA", "6").and()
                .like("EMAIL", "JOU", SqlBuilder.LikeEnum.CONTAINS).and()
                .like("PP", "JOU", SqlBuilder.LikeEnum.START_WITH).and()
                .like("RR", "JOU", SqlBuilder.LikeEnum.END_WITH).and()
                .notIn("HH", Arrays.asList("E", "F", "G")).and()
                .in("CA", Arrays.asList("A", "B", "C")).and()
                .notEqual("NA", "")
                .page(1, 2)
                .orderBy("AGE", SqlBuilder.SortOrderEnum.ASC)
                .toString();

        return sql;
    }

}
