package com.jourwon.spring.boot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author JourWon
 * @date 2023/4/23
 */
@Getter
@AllArgsConstructor
public enum DataSourceNameEnum {

    MASTER("master", "主数据源"),
    SLAVE("slave", "从数据源");

    private String dataSourceName;

    private String desc;


}
