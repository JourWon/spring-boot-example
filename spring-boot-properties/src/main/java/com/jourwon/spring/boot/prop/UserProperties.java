package com.jourwon.spring.boot.prop;

import com.jourwon.spring.boot.model.entity.Pet;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;


/**
 * 用户
 *
 * @author JourWon
 * @date 2023/3/1
 */
@Data
@ConfigurationProperties(prefix = "my-project")
public class UserProperties {

    private Long userId;

    private String username;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate birthday;

    private boolean man;

    private List<Pet> petList;

    private List<String> stringList;

    private String[] stringArray;

    private Map<String, String> stringMap;

    private Map<String, Pet> petMap;

}
