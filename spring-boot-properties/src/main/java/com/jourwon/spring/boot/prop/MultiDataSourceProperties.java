package com.jourwon.spring.boot.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 多数据源配置
 *
 * @author JourWon
 * @date 2023/4/1
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "my-project.multi-data-source")
public class MultiDataSourceProperties {

    /**
     * mysql数据源配置
     */
    private DataSourceProperties mysql = mysqlDataSourceProperties();

    /**
     * postgresql数据源配置
     */
    private DataSourceProperties pgsql = pgsqlDataSourceProperties();

    /**
     * 数据源配置
     */
    @Getter
    @Setter
    public static class DataSourceProperties {

        /**
         * 数据源类型
         */
        private String type;

        /**
         * url
         */
        private String url;

        /**
         * 驱动类名
         */
        private String driverClassName;

        /**
         * 用户名
         */
        private String username;

        /**
         * 密码
         */
        private String password;

    }

    /**
     * mysql数据源默认配置
     *
     * @return DataSourceProperties 数据源配置
     */
    private DataSourceProperties mysqlDataSourceProperties() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setType("com.zaxxer.hikari.HikariDataSource");
        dataSourceProperties.setUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&allowMultiQueries=true&useSSL=false&tinyInt1isBit=false&serverTimezone=GMT%2B8");
        dataSourceProperties.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceProperties.setUsername("root");
        dataSourceProperties.setPassword("root");
        return dataSourceProperties;
    }

    /**
     * pgsql数据源默认配置
     *
     * @return DataSourceProperties 数据源配置
     */
    private DataSourceProperties pgsqlDataSourceProperties() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setType("com.alibaba.druid.pool.DruidDataSource");
        dataSourceProperties.setUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSourceProperties.setDriverClassName("org.postgresql.Driver");
        dataSourceProperties.setUsername("postgres");
        dataSourceProperties.setPassword("postgres");
        return dataSourceProperties;
    }

}
