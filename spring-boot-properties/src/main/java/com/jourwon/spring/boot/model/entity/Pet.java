package com.jourwon.spring.boot.model.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 宠物
 *
 * @author JourWon
 * @date 2023/3/2
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pet implements Serializable {

    private static final long serialVersionUID = -2867828121075809974L;

    @ApiModelProperty(value = "宠物年龄")
    private Long age;

    @ApiModelProperty(value = "宠物名")
    private String name;

}
