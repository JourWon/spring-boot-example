// package com.jourwon.spring.boot.config;
//
// import lombok.Data;
// import org.springframework.boot.context.properties.ConfigurationProperties;
//
// import java.util.Map;
//
// /**
//  * Httpclient 的线程池配置
//  * java配置的优先级低于yml配置；如果yml配置不存在，会采用java配置
//  *
//  * @author JourWon
//  * @date 2022/6/19
//  */
// @Data
// @ConfigurationProperties(prefix = "http-client.pool")
// public class HttpClientPoolProperties {
//
//     /**
//      * 同路由的并发数
//      */
//     private Integer maxPerRoute = 10;
//
//     /**
//      * 连接池的最大连接数
//      */
//     private Integer maxTotal = 20;
//
//     /**
//      * 从连接池获取连接的超时时间,不宜过长,单位毫秒，默认500毫秒
//      */
//     private Integer connectionRequestTimout = 500;
//
//     /**
//      * 客户端和服务器建立连接的超时时间，默认3s
//      */
//     private Integer connectTimeout = 3_000;
//
//     /**
//      * 客户端和服务器建立连接后，客户端从服务器读取数据的超时时间，超出后会抛出SocketTimeOutException，默认10s
//      */
//     private Integer readTimeout = 10_000;
//
//     /**
//      * 重试次数,默认2次
//      */
//     private Integer retryCount = 2;
//
//     /**
//      * 针对不同的地址,特别设置不同的长连接保持时间
//      */
//     private Map<String, Integer> keepAliveTargetHost;
//
//     /**
//      * 针对不同的地址,特别设置不同的长连接保持时间,单位毫秒，默认60s
//      */
//     private Integer keepAliveTime = 60_000;
//
//     /**
//      * 最大空闲时间，单位毫秒，默认120s
//      */
//     private Integer maxIdleTime = 120_000;
//
// }
