package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author JourWon
 * @date 2022/4/21
 */
@Api(tags = "Redis测试")
@RestController
@RequestMapping("/redis")
public class RedisController {

    @Resource
    private RedisUtils redisUtils;

    @GetMapping("/get-key-value")
    @ApiOperation("获取key对应的值")
    public String getKeyValue(@RequestParam String key) {
        return redisUtils.get(key);
    }

}
