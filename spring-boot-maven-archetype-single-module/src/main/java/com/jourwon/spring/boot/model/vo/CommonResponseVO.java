package com.jourwon.spring.boot.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jourwon.spring.boot.constant.SystemConstants;
import com.jourwon.spring.boot.enums.CommonResponseCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 统一响应对象
 *
 * @author JourWon
 * @date 2021/1/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CommonResponseVO-统一响应对象")
public class CommonResponseVO<T> implements Serializable {

    private static final long serialVersionUID = -1338376281028943181L;

    @ApiModelProperty(value = "响应编码")
    private String code;

    @ApiModelProperty(value = "响应信息")
    private String message;

    @ApiModelProperty(value = "链路id")
    private String traceId = MDC.get(SystemConstants.TRACE_ID);

    @ApiModelProperty(value = "响应日期时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime localDateTime = LocalDateTime.now();

    @ApiModelProperty(value = "业务数据")
    private T data;

    public CommonResponseVO(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public CommonResponseVO(CommonResponseCodeEnum commonResponseCodeEnum) {
        this.code = commonResponseCodeEnum.getCode();
        this.message = commonResponseCodeEnum.getMessage();
    }

    public CommonResponseVO(CommonResponseCodeEnum commonResponseCodeEnum, T data) {
        this.code = commonResponseCodeEnum.getCode();
        this.message = commonResponseCodeEnum.getMessage();
        this.data = data;
    }

    public static <T> CommonResponseVO<T> success() {
        return new CommonResponseVO<>(CommonResponseCodeEnum.SUCCESS);
    }

    public static <T> CommonResponseVO<T> success(T data) {
        return new CommonResponseVO<>(CommonResponseCodeEnum.SUCCESS, data);
    }

    public static <T> CommonResponseVO<T> success(CommonResponseCodeEnum commonResponseCodeEnum, T data) {
        return new CommonResponseVO<>(commonResponseCodeEnum, data);
    }

    public static <T> CommonResponseVO<T> failure() {
        return new CommonResponseVO<>(CommonResponseCodeEnum.SYSTEM_EXCEPTION);
    }

    public static <T> CommonResponseVO<T> failure(CommonResponseCodeEnum commonResponseCodeEnum) {
        return new CommonResponseVO<>(commonResponseCodeEnum);
    }

}
