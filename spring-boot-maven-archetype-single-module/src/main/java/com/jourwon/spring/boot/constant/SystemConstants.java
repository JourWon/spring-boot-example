package com.jourwon.spring.boot.constant;

/**
 * 系统常量类
 *
 * @author JourWon
 * @date 2022/6/26
 */
public class SystemConstants {

    /**
     * 链路id
     */
    public static final String TRACE_ID = "traceId";

}
