package com.jourwon.spring.boot.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Base64;

/**
 * base64工具类
 * 字符串进行 Base64 编解码工具类
 * 文件与 Base64 字符串转换工具类
 *
 * @author JourWon
 * @date 2022/6/20
 */
public class Base64Utils {

    /**
     * Encoder方式编码
     *
     * @param source 源字符串
     * @return String 编码后的字符串
     */
    public static String encode(String source) {
        return Base64.getEncoder().encodeToString(source.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Encoder方式解码
     *
     * @param source 源字符串
     * @return String 解码后的字符串
     */
    public static String decode(String source) {
        byte[] base64decodedBytes = Base64.getDecoder().decode(source);
        return new String(base64decodedBytes, StandardCharsets.UTF_8);
    }

    /**
     * UrlEncoder方式编码
     *
     * @param source 源字符串
     * @return String 编码后的字符串
     */
    public static String urlEncode(String source) {
        return Base64.getUrlEncoder().encodeToString(source.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * UrlEncoder方式解码
     *
     * @param source 源字符串
     * @return String 解码后的字符串
     */
    public static String urlDecode(String source) {
        byte[] base64decodedBytes = Base64.getUrlDecoder().decode(source);
        return new String(base64decodedBytes, StandardCharsets.UTF_8);
    }

    /**
     * MimeEncoder方式编码
     *
     * @param source 源字符串
     * @return String 编码后的字符串
     */
    public static String mimeEncode(String source) {
        byte[] mimeBytes = source.getBytes(StandardCharsets.UTF_8);
        return Base64.getMimeEncoder().encodeToString(mimeBytes);
    }

    /**
     * MimeEncoder方式解码
     *
     * @param source 源字符串
     * @return String 解码后的字符串
     */
    public static String mimeDecode(String source) {
        byte[] base64decodedBytes = Base64.getMimeDecoder().decode(source);
        return new String(base64decodedBytes, StandardCharsets.UTF_8);
    }

    /**
     * 文件转化成base64字符串
     *
     * @param path 文件路径
     * @return String base64字符串
     */
    public static String fileToBase64Str(String path) {
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(path);
            data = new byte[in.available()];
            in.read(data);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // 对字节数组Base64编码
        return Base64.getMimeEncoder().encodeToString(data);
    }

    /**
     * 将base64字符串转换为文件并存储到指定位置
     *
     * @param base64Str base64字符串
     * @param filePath  文件路径
     * @return boolean true表示转换成功，false表示转换失败
     */
    public static boolean base64StrToFile(String base64Str, String filePath) {
        if (base64Str == null && filePath == null) {
            return false;
        }
        try {
            Files.write(Paths.get(filePath), Base64.getMimeDecoder().decode(base64Str), StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}
