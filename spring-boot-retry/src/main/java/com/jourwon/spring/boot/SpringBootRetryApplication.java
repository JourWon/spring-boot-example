package com.jourwon.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2022/6/19
 */
@SpringBootApplication
public class SpringBootRetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRetryApplication.class, args);
    }

}
